const gulp = require('gulp');
const ts = require('gulp-typescript');


// Clean tasks

gulp.task('clean', function() {
    const del = require('del');
    return del('dist');
});


// Build tasks

gulp.task('ts', function() {
    return gulp.src('src/**/*.ts')
        .pipe(ts({
            target: 'ES2015',
            module: 'commonjs',
            declaration: true,
            noImplicitAny: true,
         }))
        .pipe(gulp.dest('dist'));
});

gulp.task('build', gulp.series('ts'));
gulp.task('default', gulp.series('build'));
