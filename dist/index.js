"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const microservice_1 = require("microservice");
const microservice_orchestrator_1 = require("microservice-orchestrator");
const minimist = require("minimist");
const os = require("os");
/**
 * Main entrypoint to start this service
 * @param opts Catch-all for parameters. Options include:
 *
 * `port` - TCP port where this service should run
 *
 * `db` - database location, defaults to in-memory database
 *
 * `orchestrator` - URL where microservice-orchestrator is running; must be full URL including
 * protocol. E.g. "http://127.0.0.1:3000"
 */
function main(opts = {}) {
    return __awaiter(this, void 0, void 0, function* () {
        // Initialize status
        const GLOBAL_STATS = {};
        GLOBAL_STATS.hostname = os.hostname();
        /**
         * Helper function used to read arguments. The order of preference is: 1) opts 2) args 3) env
         * @param name name of the parameter being retrieved
         * @param env if true, search for parameter setting in ENV as well
         */
        function getopt(name, env = true) {
            return opts[name] || args[name] || (env && process.env[name]);
        }
        // Parse command-line arguments and define global parameters
        const args = minimist(process.argv.slice(1));
        // Initialize microservice
        const service = yield microservice_1.MicroService.create(getopt('db'));
        // Setup debugging
        process.on('unhandledRejection', err => service.log('E', err));
        // Routes setup
        // Root
        yield service.route('/', _ => 'Hello World');
        // Echoes second part of path
        yield service.route('/echo/:message', (request, params) => params.message);
        // Echoes "message" from query parameters
        yield service.route({ path: '/echo', method: 'GET', mandatoryQueryParameters: ['message'] }, request => request.path.split('message=')[1]);
        // Maintenance loop
        const maintenance_loop = () => __awaiter(this, void 0, void 0, function* () {
            const rows = yield service.db.all(`SELECT timestamp, message FROM log WHERE level = 'E' ORDER BY timestamp DESC LIMIT 10`);
            const msgs = rows.map((row) => row.message);
            GLOBAL_STATS.last_errors = rows
                .filter((row, ix) => msgs.indexOf(row.message) === ix)
                .map((row) => `[${row.timestamp}] ${row.message}`);
            GLOBAL_STATS.loadavg = os.loadavg();
        });
        setInterval(maintenance_loop, 60000);
        yield maintenance_loop();
        // Pick a random port
        service.start(parseInt(getopt('port')) || Math.floor(Math.random() * 8975) + 1024);
        // Register with orchestrator if option was passed as argument
        if (getopt('orchestrator')) {
            const name = 'template';
            const orchestrator_url = new URL(getopt('orchestrator'));
            yield microservice_orchestrator_1.register(service, { name: name, url: orchestrator_url, routes: [
                    { path: '/echo', remotePath: '/' },
                    { path: '/echo/:message', remotePath: '/echo/:message' }
                ] });
        }
        // Notify other components of startup, but do not exit on failure
        yield service.notify().catch(err => { });
        return service;
    });
}
exports.main = main;
// If starting from the command line, begin execution
if (typeof require !== 'undefined' && require.main === module) {
    main();
}
