"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const microservice_1 = require("microservice");
const assert = require("assert");
const index_1 = require("../index");
let service;
describe('routes', () => {
    before(() => __awaiter(this, void 0, void 0, function* () {
        service = yield index_1.main();
    }));
    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(Math.floor(Math.random() * 8975) + 1024);
    });
    afterEach(() => {
        service.stop();
    });
    describe('root', () => {
        it('OK', () => __awaiter(this, void 0, void 0, function* () {
            const res = yield new microservice_1.HttpRequest(service.server).get('/');
            assert.equal(res.statusCode, 200);
        }));
    });
    describe('echo', () => {
        it('repeat query', () => __awaiter(this, void 0, void 0, function* () {
            const rnd = '' + Math.floor(Math.random() * 9999);
            const res = yield new microservice_1.HttpRequest(service.server).get('/echo?message=' + rnd);
            assert.equal(res.body, rnd);
        }));
        it('repeat param', () => __awaiter(this, void 0, void 0, function* () {
            const rnd = '' + Math.floor(Math.random() * 9999);
            const res = yield new microservice_1.HttpRequest(service.server).get('/echo/' + rnd);
            assert.equal(res.body, rnd);
        }));
    });
});
