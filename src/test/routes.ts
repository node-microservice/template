import { MicroService, HttpRequest } from 'microservice'
import * as assert from 'assert'
import { main } from '../index'
let service: MicroService

describe('routes', () => {

    before(async () => {
        service = await main()
    })

    beforeEach(() => {
        service.stop()
        service.clearCache(true)
        service.start(Math.floor(Math.random() * 8975) + 1024)
    })
    afterEach(() => {
        service.stop()
    })

    describe('root', () => {

        it('OK', async () => {
            const res = await new HttpRequest(service.server).get('/')
            assert.equal(res.statusCode, 200)
        })
    })

    describe('echo', () => {

        it('repeat query', async () => {
            const rnd = '' + Math.floor(Math.random() * 9999)
            const res = await new HttpRequest(service.server).get('/echo?message=' + rnd)
            assert.equal(res.body, rnd)
        })

        it('repeat param', async () => {
            const rnd = '' + Math.floor(Math.random() * 9999)
            const res = await new HttpRequest(service.server).get('/echo/' + rnd)
            assert.equal(res.body, rnd)
        })
    })
});