# MicroService Template

Reference implementation of a microservice using [microservice](https://gitlab.com/node-microservice/microservice).

## Installation

Installing this package globally also installs a SystemD template unit file. To start the SystemD service then, run the following commands as sudo:

    npm install -g gitlab:node-microservice/template
    systemctl daemon-reload
    systemctl enable microservice-template@1234
    systemctl start microservice-template@1234

Substitute 1234 with the port under which you want to run the microservice orchestrator.

## Usage

This project is not very useful as-is. The recommended approach is to fork it and then modify the routes according to your needs.
